
Preparation "Kotlin Multiplatform" Workshop
======


[1. Installation](#1-installation)
* [1.1 Java](#11-java)
* [1.2 Android Studio Preview](#12-android-atudio-preview)
* [1.3 Gradle](#13-gradle)
* [1.4 Visual Studio Code](#14-visual-studio-code)
* [1.5 git](#15-git)
* [1.6 Chrome](#16-chrome)
* [1.7 Node](#17-node)
* [1.8 YARN](#18-yarn)
* [1.9 XCode (only Mac users)](#19-xcode-(only-mac-users))

[2. Workshop slides and sample code](#2-workshop-slides-and-sample-code)

# 1. Installation

## 1.1 Java

```
//check if already installed (version must be 8 or higher)
java -version
```

```
//Link to download 
 https://openjdk.java.net 
```

* add java to the **path**

## 1.2 Android Studio Preview
**Android Studio 4.2 Canary 7 Version from 30 July 2020**
* Download and unzip or use bundled installer 

```
//Link to download
 https://developer.android.com/studio/archive
```

![ Android Studio Preview](Android_Studio_canary7.png)

## 1.3 Gradle
 
```
//check if already installed (required version 6.5.1 or higher)
gradle -v" 
```

```
//Link to download 
https://gradle.org/install/ 
```

* add to the **path**

## 1.4 Visual Studio Code

```
//Link to download 
https://code.visualstudio.com/download
```

## 1.5 git

```
//check if already installed
git --version
```
```
//Link to download 
https://git-scm.com
```

* add to the **path**

## 1.6 Chrome

```
//Link to download 
https://www.google.com/intl/de/chrome/
```

## 1.7 Node

```
//check if already installed 
node -v
```

```
//Link to download 
https://nodejs.org/de/download/
```
(for Windows use msi installer , npm will be part of the installation)

* add to the **path**

## 1.8 YARN
```
* check if installed with 
yarn --version
```

```
//Link to download 
https://classic.yarnpkg.com/en/
```
(for Windows use:
https://classic.yarnpkg.com/en/docs/install/#windows-stable )



## 1.9 XCode (only Mac users)

* check if already installed (required XCode Version >= 11.4)

```
//Link to download 
https://www.apple.com/ios/app-store/
```


# 2. Workshop slides and sample code

The Workshop documentation, slides , sample codes and templates can be found in the **Gitlab** repository under the link below:

```
https://gitlab.com/mbenhajla/kmp_workshop_docs
```

**Access** to the workshop repo, will be granted through **eMail Invitation** for all attendees **shortly before** the workshop ( a **free** Github or Gitlab Account will be needed to access the repo.)